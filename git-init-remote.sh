#!/bin/bash

repo=$1
token=insert_your_token_here

test -z $repo && echo "Repo name required." 1>&2 && exit 1

curl -H "Content-Type:application/json" https://gitlab.com/api/v3/projects?private_token=$token -d "{ \"name\": \"$repo\" }"

