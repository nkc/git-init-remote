Original code by  drumsrgr8forn8  found here https://coderwall.com/p/r7yh6g/create-a-new-gitlab-repo-from-the-command-line. I thought I would bring gitlab specific code to git lab.


Create a script and name it something like git-init-remote

Note: I have used GitLab's free instance they now provide. Change the url to your own if you're running a private instance of GitLab

#!/bin/bash

repo=$1
token=put_your_api_token_here

test -z $repo && echo "Repo name required." 1>&2 && exit 1

curl -H "Content-Type:application/json" https://gitlab.com/api/v3/projects?private_token=$token -d "{ \"name\": \"$repo\" }"

Run chmod +x <script_name> . And move it somewhere into you PATH.

Now you can run git init-remote my-cool-repo to create a new repo. You'll get a bunch of json back... Someone could probably write a fancier script that actually checks for success or failure etc, please feel free to branch this.
